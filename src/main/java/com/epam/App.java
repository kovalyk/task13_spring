package com.epam;

import com.epam.config.BeanConfigTwo;
import com.epam.model.BeanA;
import com.epam.model.BeanB;
import com.epam.model.BeanC;
import com.epam.model.BeanD;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigTwo.class);
//        BeanB beanB = context.getBean(BeanB.class);
//        BeanC beanC = context.getBean(BeanC.class);
//        BeanD beanD = context.getBean(BeanD.class);
//        System.out.println(beanB +"\n" + beanC +"\n"+ beanD);
//
        System.out.println("\nResult ");
        Arrays.stream(context.getBeanDefinitionNames())
                .forEach(System.out::println);
        System.out.println();
//        PrintBeans printBeans = new PrintBeans();
//        printBeans.printBeans();

    }
}
