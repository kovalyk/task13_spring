package com.epam.config;

import com.epam.model.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

@Configuration
@Import(BeanConfigOne.class)
public class BeanConfigTwo {
    @Bean(value = "firstVariation")

    public BeanA getFirstVariation(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB.getAge(), beanC.getName());
    }

    @Bean("secondVariation")

    public BeanA getSecondVariation(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB.getAge(), beanD.getName());
    }

    @Bean("thirdVariation")
    public BeanA getThirdVariation(BeanC beanC, BeanD beanD) {
        return new BeanA(beanC.getAge(), beanD.getName());
    }

    @Bean("firstBeanE")
    public BeanE getFirstBeanE(BeanA firstVariation) {
        return new BeanE(firstVariation.getAge(), firstVariation.getName());
    }
    @Bean("secondBeanE")
    public BeanE getSecondBeanE(BeanA secondVariation) {
        return new BeanE(secondVariation.getAge(), secondVariation.getName());
    }
    @Bean("thirdBeanE")
    public BeanE getThirdBeanE(BeanA thirdVariation) {
        return new BeanE(thirdVariation.getAge(), thirdVariation.getName());
    }
}
