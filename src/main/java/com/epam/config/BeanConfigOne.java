package com.epam.config;

import com.epam.model.BeanB;
import com.epam.model.BeanC;
import com.epam.model.BeanD;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("values.properties")
public class BeanConfigOne {
    @Value("${beanB.age}")
    private int ageB;
    @Value("${beanB.name}")
    private String nameB;
    @Value("${beanC.age}")
    private int ageC;
    @Value("${beanC.name}")
    private String nameC;
    @Value("${beanD.age}")
    private int ageD;
    @Value("${beanD.name}")
    private String nameD;

    @Bean(value = "beanC")
    @DependsOn(value = "beanB")
    public BeanC getBeanC() {
        return new BeanC(ageC, nameC);
    }


    @Bean(value = "beanB")
    @DependsOn(value = "beanD")
    public BeanB getBeanB() {
        return new BeanB(ageB, nameB);
    }

    @Bean(value = "beanD")
    public BeanD getBeanD() {
        return new BeanD(ageD, nameD);
    }

}
