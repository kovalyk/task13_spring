package com.epam.model;

public class BeanA implements BeanValidator {
    private int age;
    private String name;

    public BeanA(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void validate() {

    }

    @Override
    public String toString() {
        return "BeanA{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
