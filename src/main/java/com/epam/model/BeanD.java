package com.epam.model;

public class BeanD implements BeanValidator{

    private int age;
    private String name;

    public BeanD(int age, String name) {
        this.age = age;
        this.name = name;
        System.out.println("Constructor of BeanD");

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void validate() {

    }

    @Override
    public String toString() {
        return "BeanD{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}
