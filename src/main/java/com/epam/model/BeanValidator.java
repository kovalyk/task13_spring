package com.epam.model;

@FunctionalInterface
public interface BeanValidator {
    void validate();
}
